(define (problem task3)
	(:domain logistics)
	(:objects truck1 tampinese changi bedok p1 p2)
	(:init
		(truck truck1)
		(location tampinese)
		(location changi)
		(location bedok)
		(package p1)
		(package p2)
		(truck-at-location truck1 tampinese)
		(package-at-location p1 bedok)
		(package-at-location p2 changi)
	)
	(:goal (and
		(package-at-location p1 changi)
		(package-at-location p2 bedok)
	))
)