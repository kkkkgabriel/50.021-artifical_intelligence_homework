(define (problem task2)
	(:domain logistics)
	(:objects truck1 tampinese changi bedok p1)
	(:init
		(truck truck1)
		(location tampinese)
		(location changi)
		(location bedok)
		(package p1)
		(truck-at-location truck1 tampinese)
		(package-at-location p1 bedok)
	)
	(:goal (and (package-at-location p1 changi)))
)