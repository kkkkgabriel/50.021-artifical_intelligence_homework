(define (domain logistics)
	(:predicates 
		(truck ?t)
		(location ?l)
		(package ?p)
		(truck-at-location ?t ?l)
		(package-at-location ?p ?l)
		(package-on-truck ?p ?t)
	)

	(:action move
		:parameters (?t ?origin ?dest)
		:precondition (and 
			(location ?origin) 
			(location ?dest) 
			(truck-at-location ?t ?origin)
		)
		:effect (and 
			(truck-at-location ?t ?dest)
			(not (truck-at-location ?t ?origin))
		)
	)

	(:action pick
		:parameters (?p ?t ?l)
		:precondition (and
			(not (package-on-truck ?p ?t))
			(package-at-location ?p ?l)
			(truck-at-location ?t ?l)
		)
		:effect (and
			(package-on-truck ?p ?t)
			(not (package-at-location ?p ?l))
		)
	)

	(:action drop
		:parameters (?p ?t ?l)
		:precondition (and
			(package-on-truck ?p ?t)
			(not (package-at-location ?p ?l))
			(truck-at-location ?t ?l)
		)
		:effect (and
			(not (package-on-truck ?p ?t))
			(package-at-location ?p ?l)
		)
	)
)