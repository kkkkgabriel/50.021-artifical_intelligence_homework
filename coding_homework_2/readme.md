# Instructions 
- Run semi-magic.py for experiment results
- See analysis.md for analysis of experiment results and problem solving
- Uncomment line 119 to line 129 in ```csp.py```, and run semi-magic.py to see corrected implementation of algorithms