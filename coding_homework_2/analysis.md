Done by: Gabriel Koh (1003882)

# Experiment
When selecting the next unassigned variable to explore, the minimum remaining values heuristic implements a random selection to resolve a tie between 2 variables with equal remaining values. This leads to a non-deterministic step in the algorithm. To fairly experiment the backtracking algorithm and analysize their performance, I seeded the ```random``` module, and iterated each heuristic 1000 times, defined in ```N_EXPERIMENTS```.
Running ```semi-magic.py```, the following would be the given output from the script:

# Observations
The following is the output of the semi-magic.py script.
> Average number of assignments for backtracking = 9.0
> --- 0.8165786266326904 seconds ---
> 
> Average number of assignments for backtracking with forward checking = 9.0
> --- 0.9036881923675537 seconds ---
> 
> Average number of assignments for backtracking with lcv = 9.0
> --- 0.9737122058868408 seconds ---
> 
> Average number of assignments for backtracking with mrv = 13.542
> --- 1.8325939178466797 seconds ---
> 
> Average number of assignments for backtracking with mrv + lcv = 13.362666666666666
> --- 2.3598544597625732 seconds ---
> 
> Average number of assignments for backtracking with mrv + mac = 9.0
> --- 2.094895839691162 seconds ---
> 
> Average number of assignments for backtracking with mrv + forward checking = 9.0
> --- 1.3011174201965332 seconds ---
> 
> Average number of assignments for backtracking with mrv + lcv + mac = 9.0
> --- 2.198852777481079 seconds ---

From here, we can see that the pure backtracking algorithm, backtracking with LCV, backtracking with forward checking, backtracking with mrv + mac and backtracking with MRV + LCV + MAC all constantly required 9 assignments to solve the problem, which is the minimum number of assignments to a 9 variable problem. <br>
On the other hand, backtracking with MRV and backtracking with MCV + LCV required 13.54 and 13.36 assignments on average respectively.<br>

# Analysis
## Comparing pure backtracking vs backtracking with MRV
Ideally, we would expect backtracking with MRV to perform better, if not as good as pure backtracking. However, this is not the case. This is possibly because of the (pseudo) randomness in the MRV function when resolving a tie. There are chances where the selected variable is part of a wrong path, causing it to backtracking further down the branch, using more assignments than necessary.<br><br>
On the other hand, the pure backtracking algorithm manages to constantly solve the problem with minimum assignments. This could be because the order that the variables are selected to assign are coincidentally in the path of the solution.

## Comparing backtracking with MRV vs backtracking with LCV
As mentioned, backtracking with MRV occasionally selects a wrong path to explore due to the random selection during a tie. LCV does not reduce the number of wrong selected variables by MRV, but improves the chances of assigning the correct value when a correct variable is selected. Nevertheless, LCV does not resolve the MRV problem.

## Comparing backtracking with MRV vs backtracking with MRV and MAC vs backtracking with MRV and forward checking
Forward checking and AC3 algorithm resolves the MRV's issue in variable selection issue by confirming that the path is fail proof before assigning the variable. AC3 is more thorough in the checking as it propagates through the possibilies and check for failures, this propogation causes an increase in runtime in finding a solution.<br>

## Deeper look into MRV implementation
In this problem, MRV should be able to find the solution without reassignments due to the nature of the constraints: After any 2 first assignments within a constraint block, variables with only single remaining values will show up, showing a straight path to the solution without fail. Its strange that the MRV implementation occasionally gives non-optimal solution paths, even with its non-deterministic feature.

```
def backtrack(assignment):
    if len(assignment) == len(csp.vars):
        return assignment
    var = select_unassigned_variable(assignment, csp)
    for value in order_domain_values(var, assignment, csp):
        if 0 == csp.nconflicts(var, value, assignment):
            csp.assign(var, value, assignment)
            print(var, value)
            removals = csp.suppose(var, value)
            print(removals)
            if inference(csp, var, value, assignment, removals):
                result = backtrack(assignment)
                if result is not None:
                    return result
            csp.restore(removals)
    csp.unassign(var, assignment)
    return None

def mrv(assignment, csp):
    "Minimum-remaining-values heuristic."
    return argmin_random_tie(
        [v for v in csp.vars if v not in assignment],
        lambda var: num_legal_values(csp, var, assignment))

def num_legal_values(csp, var, assignment):
    if csp.curr_domains:
        return len(csp.curr_domains[var])
    else:
        return count_if(lambda val: csp.nconflicts(var, val, assignment) == 0,
                        csp.domains[var])

def suppose(self, var, value):
    "Start accumulating inferences from assuming var=value."
    self.support_pruning()
    removals = [(var, a) for a in self.curr_domains[var] if a != value]
    self.curr_domains[var] = [value]
    return removals                       
```                     

Looking at the backtrack algorithm, variables for assignment are selected with the ```mrv``` function, which calls the ```num_legal_values``` function. However, the ```num_legal_values``` function merely compares the domains of each variable. This domain is not updated anywhere throughout the algorithm, leaving the domain of all variables to be 3 throughout, causing the MRV algorithm to not perform as to how we expect it to. The suppose class function from csp should be the function to update the domains for each variable, but it does not.<br>

The following function is the corrected ```suppose``` function.
```
def suppose(self, var, value):
    self.support_pruning()
    removals = [(var, a) for a in self.curr_domains[var] if a != value]
    self.curr_domains[var] = [value]
    for neighbor in self.neighbors[var]:
        try:
            self.curr_domains[neighbor].remove(value)
            removals.append((neighbor, value))
        except:
            pass
    return removals
```
With the new suppose function, the following are the results of all the experiments. The backtracking algorithm and any added heuristic would find the most optimal path. The corrected suppose function is left commented in ```csp.py```. Uncomment it to get the following outputs.

> Average number of assignments for backtracking = 9.0
> --- 0.28024911880493164 seconds ---
> 
> Average number of assignments for backtracking with forward checking = 9.0
> --- 0.2909250259399414 seconds ---
> 
> Average number of assignments for backtracking with lcv = 9.0
> --- 0.3530254364013672 seconds ---
> 
> Average number of assignments for backtracking with mrv = 9.0
> --- 0.5273406505584717 seconds ---
> 
> Average number of assignments for backtracking with mrv + lcv = 9.0
> --- 0.6268062591552734 seconds ---
> 
> Average number of assignments for backtracking with mrv + mac = 9.0
> --- 0.748002290725708 seconds ---
> 
> Average number of assignments for backtracking with mrv + forward checking = 9.0
> --- 0.5839250087738037 seconds ---
> 
> Average number of assignments for backtracking with mrv + lcv + mac = 9.0
> --- 0.9090464115142822 seconds ---