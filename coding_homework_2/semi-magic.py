import pdb
from csp import *
from random import shuffle
from copy import deepcopy
import time

random.seed(10)
N_EXPERIMENTS = 1000

def solve_semi_magic(algorithm=backtracking_search, **args):
    """ From CSP class in csp.py
        vars        A list of variables; each is atomic (e.g. int or string).
        domains     A dict of {var:[possible_value, ...]} entries.
        neighbors   A dict of {var:[var,...]} that for each variable lists
                    the other variables that participate in constraints.
        constraints A function f(A, a, B, b) that returns true if neighbors
                    A, B satisfy the constraint when they have values A=a, B=b
                    """
    # Use the variable names in the figure
    # csp_vars = ['V%d'%d for d in range(1,10)]
    csp_vars = [9,2,4,6,8,3,5,1,7]
    csp_vars = ['V%d'%d for d in csp_vars]

    #########################################
    # Fill in these definitions
    possible_values = list(range(3))
    csp_domains = {var: possible_values for var in csp_vars}

    # init blocks 
    csp_blocks = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9]]

    # add the block as neighbors if number in block
    csp_neighbors = {}
    for block in csp_blocks:
        for i in block:
            if csp_neighbors.get(i):
                csp_neighbors[i] += deepcopy(block)
            else:
                csp_neighbors[i] = deepcopy(block)

    # remove duplicate neighbours
    csp_neighbors = {key: list(dict.fromkeys(neighbors)) for key, neighbors in csp_neighbors.items()}

    # remove self from neighbors
    for key, neighbors in csp_neighbors.items():
        neighbors.remove(key)
    
    # format values and keys from int form to string: 1 -> 'V1'
    csp_neighbors = {'V%d'%key: ['V%d'%neighbor for neighbor in neighbors] for key, neighbors in csp_neighbors.items()}


    def csp_constraints(A, a, B, b):
        # different_values_constraint
        return a != b

    #########################################
    
    # define the CSP instance
    csp = CSP(csp_vars, csp_domains, csp_neighbors,
              csp_constraints)


    # run the specified algorithm to get an answer (or None)
    ans = algorithm(csp, **args)
    
    # print('number of assignments', csp.nassigns)
    assign = csp.infer_assignment()
    # if assign:
    #     for x in sorted(assign.items()):
    #         print(x)
    return csp.nassigns

if __name__ == '__main__':
    # print('purebacktracking')
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic()
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking = {}'.format(avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with forward checking
    heuristics = 'forward checking'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(inference=forward_checking)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with Least-constraining-values heuristic
    heuristics = 'lcv'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(order_domain_values=lcv)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with minimum-remaining-values heuristic
    heuristics = 'mrv'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(select_unassigned_variable=mrv)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with minimum-remaining-values and least-constraining-values heuristic
    heuristics = 'mrv + lcv'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(select_unassigned_variable=mrv, order_domain_values=lcv)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with minimum-remaining-values and maintaining arc consistency
    heuristics = 'mrv + mac'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(select_unassigned_variable=mrv, inference=mac)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with minimum-remaining-values and least-constraining-values heuristic and maintaining arc consistency
    heuristics = 'mrv + forward checking'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(select_unassigned_variable=mrv, inference=forward_checking)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))

    # backtracking with minimum-remaining-values and least-constraining-values heuristic and maintaining arc consistency
    heuristics = 'mrv + lcv + mac'
    total_assignments = 0
    start_time = time.time()
    for i in range(N_EXPERIMENTS):
        total_assignments += solve_semi_magic(select_unassigned_variable=mrv, order_domain_values=lcv, inference=mac)
    avg_assignments = total_assignments/N_EXPERIMENTS
    print('\nAverage number of assignments for backtracking with {} = {}'.format(heuristics ,avg_assignments))
    print("--- %s seconds ---" % (time.time() - start_time))