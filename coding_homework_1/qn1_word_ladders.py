start_word = 'take'
goal_word = 'give'
WORDS = set(i.lower().strip() for i in open('words2.txt'))

#------- util functions ----------#
def is_valid_word(word):
	'''
	Takes in a word and returns true if the word exists in dictionary WORDS
	returns false if otherwise
	'''
	return word in WORDS

def replace_char(word, position, char):
	'''
	Replaces a character of a word at a position with another character
		params:
			word (str): word to have its character replace
			position (int): position of word to replace new character
			char (str): a single character to replace with old one with
		returns:
			(str): the new word
	'''
	new_word = list(word)
	new_word[position] = char
	return ''.join(new_word)

def find_goal(word, level=0, parent=None):
	'''
	Recursively goes replaces each character of a word until it finds the goal_word.
	All explored words will be stored in the 'tree' variable.
		params:
			word (str): word to start searching with
			level (int, optional): depth of given word
			parent(str, optional): parent of given word
		returns:
			(bool): true if word is found, false if otherwise
	'''
	# return true if goal_word is found
	if word == goal_word:
		return True

	if len(word) != len(goal_word):
		raise Exception("Sorry, length of start and goal words must be the same")
	
	# loop through each character in the goal word
	for i, char in enumerate(goal_word):
		
		# replace character in word with respective character in goal word,
		# add the newly formed word (child) into the tree and explore it
		if char != list(word)[i]:
			child = replace_char(word, i, char)
			if is_valid_word(child) and child not in tree.keys():
				tree[child] = {'depth': level+1, 'parent': word}
				return find_goal(child, level+1, parent=word)
			
	return False

'''
Traverse through the tree to form word ladder
	returns:
		ladder (list): a list of strings that form the word ladder
'''
def get_ladder():
	# init the ladder with the goal word
	ladder = [goal_word]
	
	# set the current word to be the goal word before starting the loop
	current_word = goal_word 
	
	# until we reach the goal word, iteratively get the parent of the current word and add it to the ladder
	while current_word != start_word:
		current_word = tree[current_word]['parent']
		ladder.append(current_word)
		
	# reverse the ladder so it reads from start_word to goal_word
	ladder.reverse()
	return ladder

#------------ main ---------------#
def main():
	global tree
	tree = {start_word: {0, None}}

	print('Finding ladder from "{}" to "{}"'.format(start_word, goal_word))

	try:
		if find_goal(start_word):
			print(get_ladder())
			print(tree)
		else:
			print('oh no no ladder found')
	except Exception as e: 
		print(e)
		print('oh no no ladder found')
		
if __name__ == '__main__':
	main()